PipeWire - AGL presentations
---

This repository contains presentations that I have made related to PipeWire
in the context of Automotive Grade Linux. Additionally, it contains the source
of all the graphics that I have created for these presentations.

The graphics have been made using https://www.draw.io/

Copyright & License
---
Copyright (C) 2019 Collabora Ltd.

The content of these presentations as well as the graphics are licensed under a
[Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/)

![license](https://i.creativecommons.org/l/by/4.0/88x31.png "Creative Commons Attribution 4.0 International")

This license does **NOT** apply to:
 * the Collabora logo and the presentation template
 * the picture of myself, which appears on the first slide of each presentation

These may be used only in unmodified distribution of the presentations.

In addition, the icons used in some graphics are taken from the KDE breeze icon set, which is distributed under the terms of the LGPLv3 or later.

